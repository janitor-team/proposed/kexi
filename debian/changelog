kexi (1:3.2.0-3) unstable; urgency=medium

  * Drop all the l10n breaks/replaces, no more needed after two Debian stable
    releases.
  * Remove the explicit as-needed linking, as it is done by binutils now.
  * Bump the debhelper compatibility to 13:
    - switch the debhelper build dependency to debhelper-compat 13
    - remove debian/compat
    - stop passing --fail-missing to dh_missing, as it is the default behaviour
  * Use execute_after_dh_auto_install to avoid invoking dh_auto_install
    manually.
  * Backport upstream commit 73d3fa8e7a1028c89b20c74f6444a76a722d8041 to fix
    build with glib >= 2.68; patch
    upstream_Include-glib.h-outside-of-the-extern-block.patch.
  * Add Rules-Requires-Root: no.
  * Bump Standards-Version to 4.6.0, no changes required.

 -- Pino Toscano <pino@debian.org>  Sat, 21 Aug 2021 19:21:24 +0200

kexi (1:3.2.0-2) unstable; urgency=medium

  * Use the Debian postgresql-common stuff to force the default version of
    PostgreSQL:
    - invoke /usr/share/postgresql-common/supported-versions, getting the
      default version of it
    - pass -DPostgreSQL_ADDITIONAL_VERSIONS to cmake
    - drop upstream_cmake-find-PostgreSQL-12.patch, no more needed now
  * Add the libkf5doctools-dev build dependency to install the documentation
    - update kexi-data.install accordingly
    - pass -DSHOULD_BUILD_DOC=ON to cmake to enable also the English
      documentation
  * Small copyright update.

 -- Pino Toscano <pino@debian.org>  Tue, 05 Nov 2019 08:38:43 +0100

kexi (1:3.2.0-1) unstable; urgency=medium

  * New upstream release.
  * Bump the libkdb3-dev, libkproperty3-dev, and libkreport3-dev build
    dependencies to >= 3.1.91~.
  * Update the patches:
    - upstream_Fix-build-with-Qt-5.11-missing-headers.patch: drop, backported
      from upstream
    - upstream_Install-the-application-icons.patch: drop, backported from
      upstream
    - upstream_cmake-find-PostgreSQL-11.patch: drop, backported from upstream
  * Add the configuration for the CI on salsa.
  * Bump Standards-Version to 4.4.1, no changes required.
  * Backport upstream commit a86e74fc6364f3ac4adbbf6712eb4f348e68e308 to build
    with newer KF; patch upstream_Fix-build-with-Qt-5.13.patch.
  * Backport upstream commit 88ac2b4a1386bc05f0e3965f31949039eaf4d3ff to find
    PostgreSQL 12; patch upstream_cmake-find-PostgreSQL-12.patch.

 -- Pino Toscano <pino@debian.org>  Mon, 28 Oct 2019 22:56:31 +0100

kexi (1:3.1.0-4) unstable; urgency=medium

  * Backport upstream commit 54b2bcfb2ff1af6e7cbf7bdd60aef3d7c50b2785 to find
    PostgreSQL 11; patch upstream_cmake-find-PostgreSQL-11.patch.
    (Closes: #912157)

 -- Pino Toscano <pino@debian.org>  Sun, 04 Nov 2018 17:14:23 +0100

kexi (1:3.1.0-3) unstable; urgency=medium

  * Update package descriptions according to the upstream feedback.
  * Stop removing unused keximigrate_spreadsheet.mo translations, as they are
    not shipped anymore.
  * Update Vcs-* fields.
  * Backport upstream commit 1578fcb4a0407cf368edb6ee4605c4ef9e77b6ed to fix
    build with Qt >= 5.11; patch
    upstream_Fix-build-with-Qt-5.11-missing-headers.patch. (Closes: #906624)
  * Backport upstream commit 12eed3a629a06fe54f52bf0a36b4c8b81f8e5156 to
    install the application icon; patch
    upstream_Install-the-application-icons.patch
    - update kexi-data.install for this
  * Bump Standards-Version to 4.2.1, no changes required.

 -- Pino Toscano <pino@debian.org>  Mon, 03 Sep 2018 06:53:29 +0200

kexi (1:3.1.0-2) unstable; urgency=medium

  * Upload to unstable.

 -- Pino Toscano <pino@debian.org>  Fri, 09 Mar 2018 09:35:02 +0100

kexi (1:3.1.0-1) experimental; urgency=medium

  * New upstream release.
  * Bump the libkdb3-dev, libkproperty3-dev, and libkreport3-dev build
    dependencies to >= 3.1.0~.
  * Update the patches:
    - upstream_improve-.desktop-validity.patch: drop, backported from upstream
  * Update install files.
  * Simplify watch file, and switch it to https.
  * Bump Standards-Version to 4.1.3, no changes required.
  * Bump the debhelper compatibility to 11:
    - bump the debhelper build dependency to 11~
    - bump compat to 11
    - remove --parallel for dh, as now done by default
  * Switch from dh_install to dh_missing for --fail-missing.
  * Switch Vcs-* fields to salsa.debian.org.

 -- Pino Toscano <pino@debian.org>  Thu, 08 Mar 2018 23:51:57 +0100

kexi (1:3.0.2-4) unstable; urgency=medium

  * Add also 'tr' to the old calligra-l10n languages. (Closes: #874419)

 -- Pino Toscano <pino@debian.org>  Wed, 13 Sep 2017 07:22:25 +0200

kexi (1:3.0.2-3) unstable; urgency=medium

  [ Pino Toscano ]
  * Add breeze-icon-theme-rcc also as runtime dependency, since kexi refuses
    to start otherwise (sigh)...
  * Backport upstream commit 11b6265b8d14d58921edeed519f23d0e362cd7df to make
    the kexi .desktop file more valid; patch
    upstream_improve-.desktop-validity.patch.
  * Bump Standards-Version to 4.1.0, no changes required.
  * Explicitly add the gettext build dependency.

 -- Pino Toscano <pino@debian.org>  Fri, 01 Sep 2017 22:59:44 +0200

kexi (1:3.0.2-2) unstable; urgency=medium

  * Pass -DKEXI_QTGUI_RUNTIME_AVAILABLE=OFF to cmake, to make sure a GUI/window
    system is assumed to not be present at build time.
  * Suggest kexi-web-form-widget in kexi.

 -- Pino Toscano <pino@debian.org>  Tue, 22 Aug 2017 19:48:45 +0200

kexi (1:3.0.2-1) unstable; urgency=medium

  * New upstream release. (Closes: #871257)
  * Small copyright update.
  * Drop all the pre-Jessie replaces/breaks.
  * Bump kdb, kpropert, and kreport build dependencies to 3.0.2, to ensure
    building against the latest versions.
  * Bump Standards-Version to 4.0.1, no changes required.
  * Add Breaks/Replaces for all the calligra-l10n packages prior to < 2.9.11-2,
    i.e. when kexi was split out of calligra (in Debian).
  * Upload to unstable.

 -- Pino Toscano <pino@debian.org>  Sat, 19 Aug 2017 14:14:56 +0200

kexi (1:3.0.1.1-1) experimental; urgency=low

  * Initial release.

 -- Pino Toscano <pino@debian.org>  Sat, 24 Jun 2017 20:10:10 +0200
