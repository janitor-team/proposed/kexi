Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: kexi
Source: http://www.kexi-project.org/

Files: *
Copyright: 1998-1999, Torben Weis <weis@kde.org>
           2000, 2007, David Faure <faure@kde.org>
           2000, Dawit Alemayehu <adawit@kde.org>
           2002-2004, Lucijan Busch <lucijan@gmx.at>
           2002-2003, Joseph Wenninger <jowenn@kde.org>
           2002, Peter Simonsson <psn@linux.se>
           2002, Till Busch <till@bux.at>
           2003, Daniel Molkentin <molkentin@kde.org>
           2003-2018, Jarosław Staniek <staniek@kde.org>
           2003-2015, Kexi Team <kexi@kde.org>
           2004-2011, Adam Pigg <adam@piggz.co.uk>
           2004-2005, Cedric Pasteur <cedric.pasteur@free.fr>
           2004-2008, Sebastian Sauer <mail@dipe.org>
           2005, Christian Nitschkowski <segfault_ii@web.de>
           2005-2006, Martin Ellis <martin.ellis@kdemail.net>
           2007, Olivier Goffart <ogoffart@kde.org>
           2008, Sharan Rao <sharanrao@gmail.com>
           2009, 2013, Jos van den Oever <jos@vandenoever.info>
           2010, Matus Talcik <matus.talcik@gmail.com>
           2010-2011, Nokia Corporation and/or its subsidiary(-ies).
           2011, Radosław Wicik <radoslaw@wicik.pl>
           2011, Shreya Pandit <shreya@shreyapandit.com>
           2012, Dimitrios T. Tanis <dimitrios.tanis@kdemail.net>
           2012, Friedrich W. H. Kossebau <kossebau@kde.org>
           2012-2013, Oleg Kukharchuk <oleg.kuh@gmail.com>
           2013-2014, Yue Liu <yue.liu@mail.com>
           2014, Alexander Potashev <aspotashev@gmail.com>
           2014-2015, Dmitry Kazakov <dimula73@gmail.com>
           2014, Michał Poteralski <michalpoteralskikde@gmail.com>
           2014, Roman Shtemberko <shtemberko@gmail.com>
           2014, Wojciech Kosowicz <pcellix@gmail.com>
License: LGPL-2+

Files: cmake/*
Copyright: 2003-2017, Jarosław Staniek <staniek@kde.org>
           2004-2009, Kitware, Inc.
           2006-2009, Alexander Neundorf, <neundorf@kde.org>
           2006, David Faure, <faure@kde.org>
           2006-2008, Laurent Montel, <montel@kde.org>
           2007, Matthias Kretz <kretz@kde.org>
           2007, Will Stephenson <wstephenson@kde.org>
           2009-2011, Mathieu Malaterre <mathieu.malaterre@gmail.com>
           2010, Cyrille Berger, <cberger@cberger.net>
           2013-2014, Friedrich W. H. Kossebau <kossebau@kde.org>
License: BSD-3-clause

Files: cmake/modules/CheckGlobalBreezeIcons.cpp
Copyright: 2016, Jarosław Staniek <staniek@kde.org>
License: GPL-2+

Files: cmake/modules/GetGitRevisionDescription.cmake
       cmake/modules/GetGitRevisionDescription.cmake.in
Copyright: 2009-2010, Iowa State University
License: BSL-1.0

Files: doc/*
Copyright: Jaroslaw Staniek <staniek@kde.org>
           Martin A. Ellis <martin.ellis@kdemail.net>
License: GFDL-1.2+

Files: src/core/KexiGroupButton.*
Copyright: 2007, Aurélien Gâteau <agateau@kde.org>
           2012, Jean-Nicolas Artaud <jeannicolasartaud@gmail.com>
           2012-2016, Jarosław Staniek <staniek@kde.org>
License: LGPL-2

Files: src/doc/common/*
Copyright: 2000, Frederik Fouvry
           2002, Anders Lund <anders@alweb.dk>
License: GPL-2+

Files: src/kexiutils/completer/KexiCompleter.*
       src/kexiutils/completer/KexiCompleter_p.*
       src/kexiutils/completer/private/KexiAbstractItemModel_p.h
       src/kexiutils/completer/private/KexiAbstractProxyModel_p.h
Copyright: 2011, Nokia Corporation and/or its subsidiary(-ies)
License: LGPL-GPL-nokia

Files: src/kexiutils/generate_transliteration_table.sh
       src/kexiutils/update_transliteration_table_patch.sh
       src/tools/add_column/kexi_add_column
       src/tools/add_column/kexi_add_column_gui
       src/tools/delete_column/kexi_delete_column
       src/tools/delete_column/kexi_delete_column_gui
Copyright: 2006-2007, Jarosław Staniek <staniek@kde.org>
License: GPL-2+

Files: src/kexiutils/KexiFadeWidgetEffect.*
       src/kexiutils/KexiFadeWidgetEffect_p.*
Copyright: 2008, Matthias Kretz <kretz@kde.org>
           2008, Rafael Fernández López <ereslibre@kde.org>
License: LGPL-2 or LGPL-3

Files: src/kexiutils/kmessagewidget.*
       src/kexiutils/kmessagewidget_p.*
Copyright: 2011, Aurélien Gâteau <agateau@kde.org>
           2011-2013, Jarosław Staniek <staniek@kde.org>
License: LGPL-2.1+

Files: src/migration/AlterSchemaTableModel.*
       src/migration/AlterSchemaWidget.*
       src/migration/importtablewizard.*
Copyright: 2009 Adam Pigg <adam@piggz.co.uk>
           2009-2016 Jarosław Staniek <staniek@kde.org>
License: LGPL-2

Files: src/migration/mdb/3rdparty/mdbtools/*
Copyright: 2000-2011, Brian Bruns and others
License: LGPL-2+

Files: src/plugins/forms/widgets/mapbrowser/MapBrowserFactory.*
       src/plugins/forms/widgets/mapbrowser/MapBrowserWidget.cpp
Copyright: 2011, Radosław Wicik <radoslaw@wicik.pl>
License: LGPL-2.1+

Files: src/plugins/forms/widgets/webbrowser/WebBrowserFactory.*
Copyright: 2011, Shreya Pandit <shreya@shreyapandit.com>
License: LGPL-2.1+

Files: src/plugins/reports/*
Copyright: 2007-2016, Adam Pigg <adam@piggz.co.uk>
           2011-2015, Jarosław Staniek <staniek@kde.org>
License: LGPL-2.1+

Files: src/tests/widgets/kexidbdrivercombotest.cpp
Copyright: 2005, Martin Ellis <martin.ellis@kdemail.net>
License: BSD-custom
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:
 .
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.


Files: src/widget/fields/KexiFieldListModel.*
       src/widget/fields/KexiFieldListModelItem.*
Copyright: 2010, Adam Pigg <adam@piggz.co.uk>
License: LGPL-2.1+

Files: debian/*
Copyright: 2016-2019, Pino Toscano <pino@debian.org>
License: LGPL-2+

License: BSD-3-clause
 Copyright (c) The Regents of the University of California.
 All rights reserved.
 .
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 3. Neither the name of the University nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.

License: BSL-1.0
 Boost Software License - Version 1.0 - August 17th, 2003
 .
 Permission is hereby granted, free of charge, to any person or organization
 obtaining a copy of the software and accompanying documentation covered by
 this license (the "Software") to use, reproduce, display, distribute,
 execute, and transmit the Software, and to prepare derivative works of the
 Software, and to permit third-parties to whom the Software is furnished to
 do so, all subject to the following:
 .
 The copyright notices in the Software and this entire statement, including
 the above license grant, this restriction and the following disclaimer,
 must be included in all copies of the Software, in whole or in part, and
 all derivative works of the Software, unless such copies or derivative
 works are solely in the form of machine-executable object code generated by
 a source language processor.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
 SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
 FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
 ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.

License: GFDL-1.2+
 Permission is granted to copy, distribute and/or modify this document
 under the terms of the GNU Free Documentation License, Version 1.2
 or any later version published by the Free Software Foundation;
 with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
 A copy of the license is included in the section entitled "GNU
 Free Documentation License".
 .
 On Debian systems, the full text of the GNU Free Documentation
 License version 1.2 can be found in the file
 `/usr/share/common-licenses/GFDL-1.2'.

License: GPL-2+
 On Debian systems, the complete texts of the GNU General Public License
 Version 2, and 3 can be found in "/usr/share/common-licenses/GPL-2", and
 "/usr/share/common-licenses/GPL-3"

License: LGPL-2
 On Debian systems, the complete text of the GNU Library General Public
 License Version 2 can be found in "/usr/share/common-licenses/LGPL-2".

License: LGPL-2+
 On Debian systems, the complete texts of the GNU Library General Public
 License Version 2, 2.1, and 3 can be found in
 "/usr/share/common-licenses/LGPL-2", "/usr/share/common-licenses/LGPL-2.1",
 and "/usr/share/common-licenses/LGPL-3".

License: LGPL-2.1+
 On Debian systems, the complete texts of the GNU Library General Public
 License Version 2.1, and 3 can be found in
 "/usr/share/common-licenses/LGPL-2.1", and
 "/usr/share/common-licenses/LGPL-3".

License: LGPL-3
 On Debian systems, the complete text of the GNU Library General Public
 License Version 3 can be found in "/usr/share/common-licenses/LGPL-3".

License: LGPL-GPL-nokia
 GNU Lesser General Public License Usage
 This file may be used under the terms of the GNU Lesser General Public
 License version 2.1 as published by the Free Software Foundation and
 appearing in the file LICENSE.LGPL included in the packaging of this
 file. Please review the following information to ensure the GNU Lesser
 General Public License version 2.1 requirements will be met:
 http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
 .
 In addition, as a special exception, Nokia gives you certain additional
 rights. These rights are described in the Nokia Qt LGPL Exception
 version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
 .
 GNU General Public License Usage
 Alternatively, this file may be used under the terms of the GNU General
 Public License version 3.0 as published by the Free Software Foundation
 and appearing in the file LICENSE.GPL included in the packaging of this
 file. Please review the following information to ensure the GNU General
 Public License version 3.0 requirements will be met:
 http://www.gnu.org/copyleft/gpl.html.
 .
 Other Usage
 Alternatively, this file may be used in accordance with the terms and
 conditions contained in a signed written agreement between you and Nokia.
